﻿using System;

namespace Melyakina.Lab5.Num1
{
    class Program
    {
        static double Function(double x,double k)
        {

            double f = x / (Math.Sqrt(x) + (Math.Exp(k)));
            return f;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Выберите цикл: for,while, do-while ");
            try
            {
                string line = Console.ReadLine();
           
            switch (line)
                {
                    case "for":
                        for (int x = 1; x < 11; x++)
                        {
                            for (int k = 1; k < 11; k++)
                            {
                                double t = Function(x, k);
                                Console.Write($"{Math.Round(t,5)}\t");
                               
                            } Console.WriteLine();

                        }
                        break;

                    case "while":
                        int y = 1;
                        while (y < 11)
                        {
                            int z = 1;
                            while (z < 11)
                            {
                                double t = Function(y, z);
                                Console.Write($"{Math.Round(t, 5)}\t");
                                z++;
                                
                            }Console.WriteLine();
                            y++;
                        }
                        break;

                    case "do-while":
                        int m = 1;
                        do
                        {
                            int n = 1;
                            do
                            {
                                double t = Function(m, n);
                                Console.Write($"{Math.Round(t, 5)}\t");
                                n++;
                             }
                            
                            while (n < 11);
                            Console.WriteLine();
                            m++;
                        }
                        while (m < 11);
                        
                        break;
                }

            }
            catch (Exception )
            {
                Console.WriteLine("Exception");
            }

        }
    }
}
