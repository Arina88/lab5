﻿using System;

namespace Melyakina.Lab5.Num3
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                int n = Convert.ToInt32(Console.ReadLine());

                while (n > 1)
                {
                    if (n % 2 == 0)
                    {
                        n = n / 2;
                        Console.WriteLine(n);
                    }
                    else
                    {
                        n = ((n * 3) + 1) / 2;
                        Console.WriteLine(n);
                    }
                }
                Console.WriteLine(n);
            }
            catch (FormatException)
            {
                Console.WriteLine("format exception");
            }
        }     

    }
}
